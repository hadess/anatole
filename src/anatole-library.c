// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

/**
 * SECTION:anatole-library
 * @short_description: C helpers for Lua
 * @include: anatole.h
 *
 * A collection of helpers, implemented in C, for use with #AnatoleEngine,
 * and the developer code.
 */

#include "anatole-engine.h"
#include "anatole-error.h"
#include "anatole-engine.h"
#include "library/html-unescape.h"

static GVariant *
html_unescape (AnatoleEngine *engine, GVariant *arguments, gpointer user_data)
{
	const char *s;
	g_autofree char *out;

	g_variant_get (arguments,
		       "(s)",
		       &s);

	out = html_unescape_string (s);
	return g_variant_new_string (out);
}

/**
 * anatole_engine_add_html_unescape:
 * @engine: a #AnatoleEngine
 *
 * Adds `<namespace>.html_unescape` functions to Lua.
 *
 * <example>
 *  <title>HTML unescaping example</title>
 *  <programlisting>
 * unescaped = lib.html_unescape('Test &amp;amp; Test 2')
 * print(unescaped)
 *  </programlisting>
 * </example>
 *
 * <example>
 *  <title>Output of HTML unescaping example</title>
 *  <programlisting>
 * Test & Test 2
 *  </programlisting>
 * </example>
 *
 * Returns: %TRUE on success.
 */
gboolean
anatole_engine_add_html_unescape (AnatoleEngine *engine)
{
	g_return_val_if_fail (engine != NULL, FALSE);

	return anatole_engine_add_function (engine,
					    "html_unescape",
					    html_unescape,
					    "(s)",
					    NULL, NULL);
}

static GVariant *
percent_decode (AnatoleEngine *engine, GVariant *arguments, gpointer user_data)
{
	const char *s;
	g_autofree char *out;

	g_variant_get (arguments,
		       "(s)",
		       &s);

	out = g_uri_unescape_string (s, NULL);
	return g_variant_new_string (out);
}

static GVariant *
percent_encode (AnatoleEngine *engine, GVariant *arguments, gpointer user_data)
{
	const char *s;
	g_autofree char *out;

	g_variant_get (arguments,
		       "(s)",
		       &s);

	out = g_uri_escape_string (s, NULL, FALSE);
	return g_variant_new_string (out);
}

/**
 * anatole_engine_add_percent_encoding:
 * @engine: a #AnatoleEngine
 *
 * Adds `<namespace>.percent_escape` and `<namespace>.percent_unescape`
 * functions to Lua.
 *
 * <example>
 *  <title>Percent-encoding examples</title>
 *  <programlisting>
 * encoded = lib.percent_encode('Test/Test 2')
 * print(encoded)
 * decoded = lib.percent_decode(encoded)
 * print (decoded)
 *  </programlisting>
 * </example>
 *
 * <example>
 *  <title>Output of percent-encoding examples</title>
 *  <programlisting>
 * Test%2FTest%202
 * Test/Test 2
 *  </programlisting>
 * </example>
 *
 * Returns: %TRUE on success.
 */
gboolean
anatole_engine_add_percent_encoding (AnatoleEngine *engine)
{
	gboolean ret;

	g_return_val_if_fail (engine != NULL, FALSE);

	ret = anatole_engine_add_function (engine,
					   "percent_encode",
					   percent_encode,
					   "(s)",
					    NULL, NULL);
	if (ret == FALSE)
		return ret;

	return anatole_engine_add_function (engine,
					    "percent_decode",
					    percent_decode,
					    "(s)",
					    NULL, NULL);
}
