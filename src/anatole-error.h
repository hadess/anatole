// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

#pragma once

/**
 * ANATOLE_ERROR:
 *
 * Error domain for Anatole
 */
#define ANATOLE_ERROR g_quark_from_static_string("anatole-error")

/**
 * AnatoleError:
 * @ANATOLE_ERROR_FAILED: Generic error condition for when an operation fails and no more specific AnatoleError value is defined
 *
 * Error codes returned by Anatole functions.
 */
typedef enum {
	ANATOLE_ERROR_FAILED
} AnatoleError;
