// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

#include <glib-object.h>

#pragma once

#define ANATOLE_TYPE_ENGINE anatole_engine_get_type()
G_DECLARE_FINAL_TYPE(AnatoleEngine, anatole_engine, ANATOLE, ENGINE, GObject);

/**
 * AnatoleNativeFunc:
 * @engine: the #AnatoleEngine that is executing this function
 * @arguments: (transfer none): arguments to the native function packed in a tuple
 * @user_data: user data that was passed in anatole_engine_add_function()
 *
 * #AnatoleNativeFunc is the prototype of the function to be passed to
 * anatole_engine_add_function().
 *
 * Returns: a #GVariant or %NULL
 */
typedef GVariant * (* AnatoleNativeFunc) (AnatoleEngine *engine,
					  GVariant *arguments,
					  gpointer user_data);

GType          anatole_engine_get_type     (void) G_GNUC_CONST;
AnatoleEngine *anatole_engine_new          (const char *_namespace);
gboolean       anatole_engine_load_script  (AnatoleEngine  *engine,
					    const char     *uri,
					    GError        **error);
gboolean       anatole_engine_load_script_from_path  (AnatoleEngine  *engine,
						      const char     *path,
						      GError        **error);
gboolean       anatole_engine_add_function (AnatoleEngine     *engine,
					    const char        *function_name,
					    AnatoleNativeFunc  native_func,
					    const char        *gvariant_format,
					    gpointer           user_data,
					    GDestroyNotify     destroy_data);
gboolean       anatole_engine_add_function_inspect (AnatoleEngine *engine);
gboolean       anatole_engine_add_function_finish (AnatoleEngine  *engine,
						   GError        **error);
GVariant      *anatole_engine_get_global_variable (AnatoleEngine  *engine,
						   const char     *variable_name);
gboolean       anatole_engine_call_function (AnatoleEngine     *engine,
					     const char        *function_name,
					     GVariant          *in_args,
					     GError           **error);
