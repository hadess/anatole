// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

#include <glib-object.h>

#pragma once

gboolean anatole_engine_add_percent_encoding (AnatoleEngine *engine);
gboolean anatole_engine_add_html_unescape (AnatoleEngine *engine);
