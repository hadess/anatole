// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

#include <glib.h>
#include <errno.h>
#include "library/htmlentity.h"

static gchar *
char_str (gunichar c,
          gchar   *buf)
{
  memset (buf, 0, 8);
  g_unichar_to_utf8 (c, buf);
  return buf;
}

/* ANSI HTML entities
 * http://www.w3schools.com/charsets/ref_html_ansi.asp */
static gchar *
ansi_char_str (gunichar c,
               gchar   *buf)
{
  gchar from_c[2], *tmp;

  memset (buf, 0, 8);
  from_c[0] = c;
  from_c[1] = '\0';
  tmp = g_convert (from_c, 2, "UTF-8", "Windows-1252", NULL, NULL, NULL);
  strcpy (buf, tmp);
  g_free (tmp);

  return buf;
}


/* Adapted from unescape_gstring_inplace() in gmarkup.c in glib */
char *
html_unescape_string (const char *orig_from)
{
  char *to, *from, *ret;

  /*
   * Meeks' theorem: unescaping can only shrink text.
   * for &lt; etc. this is obvious, for &#xffff; more
   * thought is required, but this is patently so.
   */
  ret = g_strdup (orig_from);

  for (from = to = ret; *from != '\0'; from++, to++) {
    *to = *from;

    if (*to == '\r') {
      *to = '\n';
      if (from[1] == '\n')
        from++;
    }
    if (*from == '&') {
      from++;
      if (*from == '#') {
        gboolean is_hex = FALSE;
        gulong l;
        gchar *end = NULL;

        from++;

        if (*from == 'x') {
          is_hex = TRUE;
          from++;
        }

        /* digit is between start and p */
        errno = 0;
        if (is_hex)
          l = strtoul (from, &end, 16);
        else
          l = strtoul (from, &end, 10);

        if (end == from || errno != 0)
          continue;
        if (*end != ';')
          continue;
        /* characters XML 1.1 permits */
        if ((0 < l && l <= 0xD7FF) ||
            (0xE000 <= l && l <= 0xFFFD) ||
            (0x10000 <= l && l <= 0x10FFFF)) {
          gchar buf[8];
          if (l >= 128 && l <= 255)
            ansi_char_str (l, buf);
          else
            char_str (l, buf);
          strcpy (to, buf);
          to += strlen (buf) - 1;
          from = end;
        } else {
          continue;
        }
      } else {
        gchar *end = NULL;

        end = strstr (from, ";");
        if (!end)
          continue;

        *to = html_entity_parse (from, end - from);
        from = end;
      }
    }
  }

  *to = '\0';

  return ret;
}
