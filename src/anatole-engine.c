// SPDX-License-Identifier: LGPL-2.1

/*
 * Copyright 2018-2019 Bastien Nocera <hadess@hadess.net>
 */

/**
 * SECTION:anatole-engine
 * @short_description: Lua-C engine
 * @include: anatole.h
 *
 * #AnatoleEngine is a way to add Lua scripting to projects that use GObject.
 * It does not provide full Lua bindings to GObject libraries, as the
 * [lgi](https://github.com/pavouk/lgi) project provides. The only functions
 * available to the Lua scripting will be functions you implement in Lua itself
 * and the ones you manually declare for use by the #AnatoleEngine.
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <gio/gio.h>

#include "anatole-error.h"
#include "anatole-engine.h"

struct _AnatoleEngine {
	GObject parent_instance;
	char *_namespace;
	GHashTable *functions;

	/* Lua */
	lua_State *L;
	luaL_Reg *library_fn;
	luaL_Reg *fn;

	/* Features to add */
	guint add_inspect : 1;

	/* State */
	guint script_loaded : 1;
	guint functions_added : 1;
};

typedef struct {
	AnatoleNativeFunc native_func;
	GVariantType *gvariant_type;
	gpointer user_data;
	GDestroyNotify destroy_data;
} FunctionData;

G_DEFINE_TYPE(AnatoleEngine, anatole_engine, G_TYPE_OBJECT);

enum {
	PROP_0,
	PROP_NAMESPACE
};

#define LUA_ENV_TABLE              "_G"
#define LUA_LIBRARY_INSPECT        "inspect"
#define LUA_LIBRARY_INSPECT_URI    "resource:///org/gnome/anatole/library/inspect.lua"

/* From https://www.lua.org/pil/24.2.3.html */
G_GNUC_UNUSED static void
stackDump (lua_State *L)
{
	int i;
	int top = lua_gettop(L);
	for (i = 1; i <= top; i++) {  /* repeat for each level */
		int t = lua_type(L, i);
		switch (t) {

		case LUA_TSTRING:  /* strings */
			printf("`%s'", lua_tostring(L, i));
			break;

		case LUA_TBOOLEAN:  /* booleans */
			printf(lua_toboolean(L, i) ? "true" : "false");
			break;

		case LUA_TNUMBER:  /* numbers */
			printf("%g", lua_tonumber(L, i));
			break;

		default:  /* other values */
			printf("%s", lua_typename(L, t));
			break;

		}
		printf("  ");  /* put a separator */
	}
	printf("\n");  /* end the listing */
}

static void
anatole_engine_load_safe_libs (lua_State *L)
{
	/* http://www.lua.org/manual/5.3/manual.html#luaL_requiref
	 * http://www.lua.org/source/5.3/linit.c.html */
	static const luaL_Reg loadedlibs[] = {
		{"_G", luaopen_base},
		/* {LUA_LOADLIBNAME, luaopen_package}, */
		/* {LUA_COLIBNAME, luaopen_coroutine}, */
		{LUA_TABLIBNAME, luaopen_table},
		/* {LUA_IOLIBNAME, luaopen_io}, */
		/* {LUA_OSLIBNAME, luaopen_os}, */
		{LUA_STRLIBNAME, luaopen_string},
		{LUA_MATHLIBNAME, luaopen_math},
		{LUA_UTF8LIBNAME, luaopen_utf8},
		{LUA_DBLIBNAME, luaopen_debug},
		{NULL, NULL}
	};
	const luaL_Reg *lib;

	for (lib = loadedlibs; lib->func; lib++) {
		luaL_requiref(L, lib->name, lib->func, 1);
		lua_pop(L, 1);
	}
}

static void
anatole_engine_set_property (GObject *object,
			     guint property_id,
			     const GValue *value,
			     GParamSpec * pspec)
{
	AnatoleEngine *engine = ANATOLE_ENGINE (object);

	switch (property_id) {
	case PROP_NAMESPACE:
		engine->_namespace = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
anatole_engine_get_property (GObject    *object,
			     guint       property_id,
			     GValue     *value,
			     GParamSpec *pspec)
{
	AnatoleEngine *engine = ANATOLE_ENGINE (object);

	switch (property_id) {
	case PROP_NAMESPACE:
		g_value_set_string (value, engine->_namespace);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
function_data_free (FunctionData *func)
{
	g_variant_type_free (func->gvariant_type);
	if (func->destroy_data)
		func->destroy_data (func->user_data);
	g_free (func);
}

static void
anatole_engine_init (AnatoleEngine *engine)
{
	engine->L = luaL_newstate ();

	/* Standard Lua libraries */
	anatole_engine_load_safe_libs (engine->L);

	engine->functions = g_hash_table_new_full (g_str_hash,
						   g_str_equal,
						   g_free,
						   (GDestroyNotify) function_data_free);
}

/**
 * anatole_engine_new:
 * @_namespace: namespace for the framework functions
 *
 * Creates an engine that will be able to run Lua code. The framework
 * for functions provided by the engine, as well as the ones added
 * manually through anatole_engine_add_function() will be in this
 * namespace.
 *
 * Returns: a new #AnatoleEngine
 */
AnatoleEngine *
anatole_engine_new (const char *_namespace)
{
	g_return_val_if_fail (_namespace != NULL, NULL);

	return ANATOLE_ENGINE (g_object_new (ANATOLE_TYPE_ENGINE,
					     "namespace", _namespace,
					     NULL));
}

/**
 * anatole_engine_add_function:
 * @engine: a #AnatoleEngine
 * @function_name: the name of the function to be added
 * @native_func: (scope notified): the native function that will implement
 *   @function_name
 * @gvariant_format: a string representation of a tuple containing the arguments
 * that your function takes.
 * @user_data: (closure): user data passed to @native_func
 * @destroy_data: (destroy): the #GDestroyNotify for @user_data
 *
 * Add a new function, in the namespace passed to anatole_engine_new(), to
 * the @engine. This will make ```namespace.function_name()``` call into
 * @native_func.
 *
 * The [GVariant text format](https://developer.gnome.org/glib/stable/gvariant-text.html) needs
 * to be a tuple with the top-level items inside that tuple representing the arguments
 * your function takes, for example:
 * <example>
 *  <title>Add a new native function</title>
 *  <programlisting>
 * anatole_engine_add_function (engine,
 *                              "add",
 *                              add_int_func,
 *                              "(xx)");
 *
 * static GVariant *
 * add_int_func (GVariant *variant)
 * {
 *        int a, b;
 *
 *        g_variant_get (variant,
 *                       "(xx)",
 *                       &a, &b);
 *
 *        return g_variant_new ("x", a + b);
 * }
 *  </programlisting>
 * </example>
 *
 * <example>
 *  <title>Using the native function</title>
 *  <programlisting>
 * result = lib.add(2, 2)
 *  </programlisting>
 * </example>
 *
 * The addition will only be effective when anatole_engine_add_function_finish()
 * has been called.
 *
 * Returns: %TRUE on success, %FALSE if invalid parameters were passed.
 */
gboolean
anatole_engine_add_function (AnatoleEngine     *engine,
			     const char        *function_name,
			     AnatoleNativeFunc  native_func,
			     const char        *gvariant_format,
			     gpointer           user_data,
			     GDestroyNotify     destroy_data)
{
	FunctionData *data;

	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);
	g_return_val_if_fail (function_name != NULL, FALSE);
	g_return_val_if_fail (native_func != NULL, FALSE);
	g_return_val_if_fail (gvariant_format == NULL ||
			      g_variant_type_string_is_valid (gvariant_format), FALSE);
	g_return_val_if_fail (g_hash_table_lookup (engine->functions, function_name) == NULL, FALSE);
	g_return_val_if_fail (!engine->functions_added, FALSE);

	data = g_new0 (FunctionData, 1);
	data->native_func = native_func;
	data->user_data = user_data;
	data->destroy_data = destroy_data;
	if (gvariant_format != NULL)
		data->gvariant_type = g_variant_type_new (gvariant_format);
	g_hash_table_insert (engine->functions,
			     g_strdup (function_name),
			     data);

	return TRUE;
}

/**
 * anatole_engine_add_function_inspect:
 * @engine: a #AnatoleEngine
 *
 * Adds a new "inspect" function that can be called from lua. It transforms
 * any Lua value into a human-readable representation, perfect for debugging
 * your Lua code.
 *
 * The addition will only be effective when anatole_engine_add_function_finish()
 * has been called.
 *
 * Returns: %TRUE on success, %FALSE if an error occurred.
 */
gboolean
anatole_engine_add_function_inspect (AnatoleEngine *engine)
{
	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);

	engine->add_inspect = TRUE;

	return TRUE;
}


static gboolean
load_script_internal (lua_State      *L,
		      const char     *uri,
		      int             nresults,
		      GError        **error);

static void
add_inspect (lua_State  *L)
{
	/* Load inspect.lua and save object in global environment table */
	lua_getglobal (L, LUA_ENV_TABLE);
	if (load_script_internal (L, LUA_LIBRARY_INSPECT_URI, LUA_MULTRET, NULL) &&
	    lua_istable (L, -1)) {
		/* Top of the stack is inspect table from inspect.lua */
		lua_getfield (L, -1, "inspect");
		/* Top of the stack is inspect.inspect */
		lua_setfield (L, -4, LUA_LIBRARY_INSPECT);
		/* <namespace>.inspect points to inspect.inspect */

		/* Save inspect table in LUA_ENV_TABLE */
		lua_setfield (L, -2, "lua-inspect");
	} else {
		g_assert_not_reached ();
	}
	lua_pop (L, 1);
}

static GVariant *
lua_to_gvariant (lua_State *L,
		 int        index);

static int
gvariant_to_lua (lua_State *L,
		 GVariant  *variant);

static int
anatole_engine_marshall (lua_State *L)
{
	AnatoleEngine *engine;
	const char *func_name;
	FunctionData *data;
	AnatoleNativeFunc func;
	g_autoptr(GVariant) variant = NULL;
	g_autoptr(GVariant) ret = NULL;
	GVariantBuilder builder;
	int n, i, nret;

	func_name = lua_tostring(L, lua_upvalueindex(1));
	lua_getglobal (L, "engine-pointer");
	engine = lua_touserdata(L, -1);

	data = g_hash_table_lookup (engine->functions, func_name);
	func = data->native_func;

	n = lua_gettop (L);
	if (data->gvariant_type != NULL) {
		g_variant_builder_init (&builder, data->gvariant_type);
		for (i = 1; i < n; i++) {
			GVariant *v;

			v = lua_to_gvariant (engine->L, i);
			if (!v)
				goto bail;
			g_variant_builder_add_value (&builder, v);
		}

		variant = g_variant_builder_end (&builder);
		g_variant_ref_sink (variant);
	}
	ret = (func) (engine, variant, data->user_data);

	nret = gvariant_to_lua (engine->L, ret);
	return nret;

bail:
	g_variant_builder_clear (&builder);
	return 0;
}

static int
luaopen_funcs (lua_State *L)
{
	GList *func_names, *l;
	AnatoleEngine *engine;

	lua_getglobal (L, "engine-pointer");
	engine = lua_touserdata(L, -1);

	lua_pushstring (L, engine->_namespace);
	lua_newtable (L);

	func_names = g_hash_table_get_keys (engine->functions);
	for (l = func_names; l != NULL; l = l->next) {
		const char *func_name = l->data;

		/* Add the function name as an upvalue for our unique callback */
		lua_pushstring(L, func_name);
		lua_pushcclosure (L, anatole_engine_marshall, 1);
		lua_setfield(L, -2, func_name);
	}
	g_list_free (func_names);

	if (engine->add_inspect)
		add_inspect (engine->L);

	return 1;
}

/**
 * anatole_engine_add_function_finish:
 * @engine: a #AnatoleEngine
 * @error: return location for a #GError, or %NULL
 *
 * This function needs to be called before functions added through anatole_engine_add_function()
 * are available to the Lua code. No new functions can be added to the
 * engine after that.
 *
 * Returns: %TRUE on success, %FALSE if an error occurred.
 */
gboolean
anatole_engine_add_function_finish (AnatoleEngine  *engine,
				    GError        **error)
{
	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);
	g_return_val_if_fail (!engine->functions_added, FALSE);

	/* Namespace library */
	lua_pushlightuserdata (engine->L, engine);
	lua_setglobal (engine->L, "engine-pointer");

	luaL_requiref (engine->L, engine->_namespace, luaopen_funcs, TRUE);
	lua_pop (engine->L, 1);

	//FIXME remove the engine-pointer?

	engine->functions_added = TRUE;

	return TRUE;
}

static gboolean
load_script_internal (lua_State      *L,
		      const char     *uri,
		      int             nresults,
		      GError        **error)
{
	g_autoptr (GFile) file = NULL;
	g_autoptr (GBytes) bytes = NULL;
	g_autoptr (GError) err = NULL;
	gconstpointer data;
	gsize size;
	int ret;

	file = g_file_new_for_uri (uri);
	if (!(bytes = g_file_load_bytes (file, NULL, NULL, &err))) {
		g_set_error (error, ANATOLE_ERROR, ANATOLE_ERROR_FAILED,
			     "Failed to load '%s': %s", uri, err->message);
		return FALSE;
	}

	data = g_bytes_get_data (bytes, &size);
	ret = luaL_loadbufferx (L, data, size, NULL, "t");
	if (ret != LUA_OK) {
		g_set_error (error, ANATOLE_ERROR, ANATOLE_ERROR_FAILED,
			     "Failed to compile '%s': %s", uri, lua_tostring (L, -1));
		return FALSE;
	}

	ret = lua_pcall (L, 0, nresults, 0);
	if (ret != LUA_OK) {
		g_set_error (error, ANATOLE_ERROR, ANATOLE_ERROR_FAILED,
			     "Failed to run '%s': %s", uri, lua_tostring (L, -1));
		return FALSE;
	}

	return TRUE;
}

/**
 * anatole_engine_load_script:
 * @engine: a #AnatoleEngine
 * @uri: URI to the Lua script
 * @error: return location for a #GError, or %NULL
 *
 * Runs the Lua script pointed at @uri. The uri and script need to be
 * valid. This will execute the code at the top-level of the script, and
 * make the functions it declares as well as the global variables
 * available from C (through anatole_engine_call_function() and
 * anatole_engine_get_global_variable() respectively).
 *
 * This can be called multiple times for the same #AnatoleEngine, which
 * makes it useful to split up your main code from helper functions.
 *
 * Returns: %TRUE on success, %FALSE if an error occurred.
 */
gboolean
anatole_engine_load_script (AnatoleEngine  *engine,
			    const char     *uri,
			    GError        **error)
{
	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);

	if (!load_script_internal (engine->L, uri, 0, error))
		return FALSE;

	engine->script_loaded = TRUE;
	return TRUE;
}

/**
 * anatole_engine_load_script_from_path:
 * @engine: a #AnatoleEngine
 * @path: local file path to the Lua script
 * @error: return location for a #GError, or %NULL
 *
 * Runs the Lua script pointed at @path. The path and script need to be
 * valid. This will execute the code at the top-level of the script, and
 * make the functions it declares as well as the global variables
 * available from C (through anatole_engine_call_function() and
 * anatole_engine_get_global_variable() respectively).
 *
 * This can be called multiple times for the same #AnatoleEngine, which
 * makes it useful to split up your main code from helper functions.
 *
 * Returns: %TRUE on success, %FALSE if an error occurred.
 */
gboolean
anatole_engine_load_script_from_path (AnatoleEngine  *engine,
				      const char     *path,
				      GError        **error)
{
	g_autofree gchar *uri = NULL;

	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);

	if (!(uri = g_filename_to_uri (path, NULL, error)))
		return FALSE;

	if (!load_script_internal (engine->L, uri, 0, error))
		return FALSE;

	engine->script_loaded = TRUE;
	return TRUE;
}

static GVariant *
lua_to_gvariant (lua_State *L,
		 int        index)
{
	switch (lua_type (L, index)) {
	case LUA_TNIL:
		/* FIXME: we can't marshal NULL GVariants, we should use
		 * a maybe GVariant to definite */
		return NULL;

	case LUA_TBOOLEAN:
		return g_variant_new_boolean (lua_toboolean (L, index));

	case LUA_TNUMBER:
		if (lua_isinteger (L, index))
			return g_variant_new_int64 (lua_tointeger (L, index));
		else
			return g_variant_new_double (lua_tonumber (L, index));

	case LUA_TSTRING:
		return g_variant_new_string (lua_tostring (L, index));

	case LUA_TTABLE: {
		GVariantDict dict;

		g_variant_dict_init (&dict, NULL);
		/* Duplicate the table as index might be relative (positive,
		 * negative or pseudo index) */
		lua_pushvalue (L, index);
		lua_pushnil (L);  /* first key */
		while (lua_next (L, -2) != 0) {
			const char *keyname;

			/* Duplicate the key as to be safe in not breaking
			 * lua_next() due some functions that could change the
			 * key type in the stack */
			lua_pushvalue (L, -2);
			/* FIXME: Need to improve key handling for the Dict as
			 * all but nil can be used as index, quoting the docs:
			 * "An associative array is an array that can be
			 * indexed not only with numbers, but also with strings
			 * or any other value of the language, except nil."
			 */
			keyname = lua_tostring (L, -1);
			if (keyname != NULL) {
				g_variant_dict_insert_value (&dict,
							     keyname,
							     lua_to_gvariant (L, -2));
			} else {
				g_warning ("Ignoring value due to unhandled key type %s in table",
					   lua_typename (L, lua_type (L, -1)));
			}
			/* removes 'value' and copy of 'key' while keeping
			 * original 'key' for next iteration */
			lua_pop (L, 2);
		}
		/* Remove duplicated table */
		lua_pop (L, 1);
		return g_variant_dict_end (&dict);
	}
	default:
		g_warning ("%s: Unhandled type %s", G_STRLOC,
			   lua_typename (L, lua_type (L, index)));
		break;
	}

	return NULL;
}

static int
gvariant_to_lua (lua_State *L,
		 GVariant  *variant)
{
	if (variant == NULL) {
		return 0;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT16)) {
		gint64 value = g_variant_get_int16 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT32)) {
		gint64 value = g_variant_get_int32 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64)) {
		gint64 value = g_variant_get_int64 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT16)) {
		guint64 value = g_variant_get_uint16 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT32)) {
		guint64 value = g_variant_get_uint32 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT64)) {
		guint64 value = g_variant_get_uint64 (variant);
		lua_pushinteger (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DOUBLE)) {
		gdouble value = g_variant_get_double (variant);
		lua_pushnumber (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BOOLEAN)) {
		gboolean value = g_variant_get_boolean (variant);
		lua_pushboolean (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING)) {
		const gchar *value = g_variant_get_string (variant, NULL);
		lua_pushstring (L, value);
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT)) {
		g_autoptr (GVariant) v = g_variant_get_variant (variant);
		return gvariant_to_lua (L, v);
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DICTIONARY)) {
		gsize n_children, i;

		n_children = g_variant_n_children (variant);
		lua_createtable (L, 0, n_children);

		for (i = 0; i < n_children; i++) {
			g_autoptr (GVariant) key, value;
			g_variant_get_child (variant, i, "{@?@*}", &key, &value);
			gvariant_to_lua (L, key);
			gvariant_to_lua (L, value);
			lua_settable (L, -3);
		}
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("a?"))) {
		gsize n_children, i;

		n_children = g_variant_n_children (variant);
		lua_createtable (L, n_children, 0);

		for (i = 0; i < n_children; i++) {
			g_autoptr (GVariant) value;
			value = g_variant_get_child_value (variant, i);
			gvariant_to_lua (L, value);
			lua_seti (L, -2, i);
		}
		return 1;
	} else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_TUPLE)) {
		gsize n_children, i;
		int n_args = 0;

		n_children = g_variant_n_children (variant);
		for (i = 0; i < n_children; i++) {
			g_autoptr (GVariant) value;
			value = g_variant_get_child_value (variant, i);
			n_args += gvariant_to_lua (L, value);
		}
		return n_args;
	} else {
		char *type_name;

		type_name = g_variant_type_dup_string (g_variant_get_type (variant));
		g_warning ("%s: unhandled type %s", __func__, type_name);
		g_free (type_name);
	}

	return 0;
}

/**
 * anatole_engine_get_global_variable:
 * @engine: a #AnatoleEngine
 * @variable_name: the Lua variable name to get
 *
 * Fetches the Lua variable named @variable_name and packs it into
 * a #GVariant. This is useful to get the entry point into your Lua
 * script.
 *
 * Returns: (transfer floating): a #GVariant, or %NULL if the variable
 * does not exist.
 */
GVariant *
anatole_engine_get_global_variable (AnatoleEngine  *engine,
				    const char     *variable_name)
{
	GVariant *ret;

	g_return_val_if_fail (engine != NULL, NULL);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), NULL);
	g_return_val_if_fail (engine->script_loaded, NULL);
	g_return_val_if_fail (variable_name != NULL, NULL);

	lua_getglobal (engine->L, variable_name);
	ret = lua_to_gvariant (engine->L, -1);
	lua_pop (engine->L, 1);
	return ret;
}

/**
 * anatole_engine_call_function:
 * @engine: a #AnatoleEngine
 * @function_name: the Lua function name to call
 * @in_args: (transfer floating): the arguments to the function as a #GVariant tuple
 * @error: return location for a #GError, or %NULL
 *
 * Call the Lua function named @function_name, passing the arguments
 * in @in_args to it. Each of the elements in the #GVariant tuple will
 * be passed as individual arguments, for example:
 *
 * <example>
 *  <title>Call a Lua function - C side</title>
 *  <programlisting>
 * anatole_engine_call_function (engine,
 *                               "test_lua_function",
 *                               g_variant_new ("(ss)", "string 1", "string 2"),
 *                               NULL);
 *  </programlisting>
 * </example>
 *
 * <example>
 *  <title>Call a Lua function - Lua side</title>
 *  <programlisting>
 * function test_lua_function(string1, string2)
 *        print(string1 .. " ".. string2)
 * end
 *  </programlisting>
 * </example>
 *
 * Note that return values from those functions are ignored. If the Lua
 * function needs to report progress, or results, you should communicate
 * them back using a native C function that you've registered using
 * anatole_engine_add_function().
 *
 * Returns: %TRUE on success, %FALSE if invalid parameters were passed.
 */
gboolean
anatole_engine_call_function (AnatoleEngine     *engine,
			      const char        *function_name,
			      GVariant          *in_args,
			      GError           **error)
{
	g_autoptr (GVariant) args = NULL;
	int nargs;
	int ret;

	g_return_val_if_fail (engine != NULL, FALSE);
	g_return_val_if_fail (ANATOLE_ENGINE (engine), FALSE);
	g_return_val_if_fail (engine->script_loaded, FALSE);
	g_return_val_if_fail (function_name != NULL, FALSE);

	if (in_args)
		args = g_variant_ref_sink (in_args);

	lua_getglobal (engine->L, function_name);
	nargs = gvariant_to_lua (engine->L, args);

	lua_gc (engine->L, LUA_GCSTOP, 0);

	ret = lua_pcall (engine->L, nargs, 0, 0);
	if (ret != LUA_OK) {
		const gchar *msg = lua_tolstring (engine->L, -1, NULL);
		lua_pop (engine->L, 1);

		g_message ("lua_pcall failed: due %s (err %d)", msg, ret);
		//*err = g_error_new_literal (GRL_CORE_ERROR, os->error_code, msg);
		//grl_lua_operations_set_source_state (L, LUA_SOURCE_FINALIZED, os);
	}

	//FIXME ret?

	lua_gc (engine->L, LUA_GCCOLLECT, 0);
	lua_gc (engine->L, LUA_GCRESTART, 0);
	return (ret == LUA_OK);
}

static void
anatole_engine_finalize (GObject *object)
{
	AnatoleEngine *engine = ANATOLE_ENGINE (object);

	g_clear_pointer (&engine->L, lua_close);
	g_hash_table_destroy (engine->functions);
	g_clear_pointer (&engine->fn, g_free);
	g_clear_pointer (&engine->_namespace, g_free);

	G_OBJECT_CLASS (anatole_engine_parent_class)->finalize (object);
}

static void
anatole_engine_class_init (AnatoleEngineClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = anatole_engine_finalize;
	gobject_class->set_property = anatole_engine_set_property;
	gobject_class->get_property = anatole_engine_get_property;

	g_object_class_install_property (gobject_class, PROP_NAMESPACE,
					 g_param_spec_string ("namespace", "Namespace", "Namespace prefix for functions",
							      "lib",
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}
