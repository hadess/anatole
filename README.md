Note: Anatole is under heavy development, and doesn't have all the features
listed actually usable. APIs might change, but the goal is having all those
features for the first public release.

# Anatole

Anatole is a library to help with embedding Lua scripts into GLib/GObject
programs and libraries. The main `AnatoleEngine` object will hide most of
Lua's C embedding quirks, presenting an API more in keeping with other
GLib APIs, such as GDBus with the use of GVariant as the data format,
and GTask as the async function support.

The library is also setup to be used as a git submodule, with the build system
integrated through the meson build system.

Note that this is not a way to access GObject-based libraries through Lua using
gobject-introspection, there's already a [project called lgi for that](https://github.com/pavouk/lgi).

## Requirements

- GLib
- Lua

Depending on the embedding application or library's needs, you could also need:
- libarchive for unzipping support
- json-glib for json decoding
- gperf for html entities encoding and decoding
- libsoup for HTTP(S) client support

## Features
