#include <locale.h>
#include "anatole.h"

static GVariant *
add_int_func (AnatoleEngine *engine,
	      GVariant *variant,
	      gpointer user_data)
{
	int a, b;

	g_variant_get (variant,
		       "(xx)",
		       &a, &b);

	return g_variant_new ("x", a + b);
}

static GVariant *
multiply_int_func (AnatoleEngine *engine,
		   GVariant *variant,
		   gpointer user_data)
{
	int a, b;

	g_variant_get (variant,
		       "(xx)",
		       &a, &b);

	return g_variant_new ("x", a * b);
}

static GVariant *
assert_equals_int_func (AnatoleEngine *engine,
			GVariant *variant,
			gpointer user_data)
{
	int n1, n2;

	g_variant_get (variant,
		       "(xx)",
		       &n1, &n2);

	g_assert_cmpint (n1, ==, n2);

	return NULL;
}

static GVariant *
string_contains_func (AnatoleEngine *engine,
		      GVariant *variant,
		      gpointer user_data)
{
       const char *haystack, *needle;

       g_variant_get (variant,
                      "(ss)",
                      &haystack, &needle);

       return g_variant_new_boolean (strstr (haystack, needle) != NULL);
}

static gboolean function_called = FALSE;

static GVariant *
function_that_could_be_called (AnatoleEngine *engine,
			       GVariant *variant,
			       gpointer user_data)
{
	g_assert (!function_called);
	function_called = TRUE;
	return NULL;
}

static GVariant *
send_asv_func (AnatoleEngine *engine,
	  GVariant *variant,
	  gpointer user_data)
{
	GVariant *dict = NULL;
	GVariant *value;

	g_variant_get (variant, "(@a{sv})", &dict);
	g_assert_nonnull (dict);

	g_assert_nonnull ((value =
		g_variant_lookup_value (dict, "number", G_VARIANT_TYPE_INT64)));
	g_variant_unref (value);

	g_assert_nonnull ((value =
		g_variant_lookup_value (dict, "string", G_VARIANT_TYPE_STRING)));
	g_variant_unref (value);

	g_assert_nonnull ((value =
		g_variant_lookup_value (dict, "string_num", G_VARIANT_TYPE_STRING)));
	g_variant_unref (value);

	g_assert_nonnull ((value =
		g_variant_lookup_value (dict, "double", G_VARIANT_TYPE_DOUBLE)));
	g_variant_unref (value);

	g_assert_nonnull ((value =
		g_variant_lookup_value (dict, "bool", G_VARIANT_TYPE_BOOLEAN)));
	g_variant_unref (value);

	g_variant_unref (dict);
	return NULL;
}

static void
check_global (AnatoleEngine *engine,
	      const char *name,
	      const char *expected_value)
{
	GVariant *variant;
	char *str;

	variant = anatole_engine_get_global_variable (engine, name);
	g_assert_nonnull (variant);
	str = g_variant_print (variant, TRUE);
	g_assert_cmpstr (str, ==, expected_value);
	g_free (str);
	g_variant_unref (variant);
}

static GVariant *
build_asv ()
{
	GVariantBuilder b;
	GVariantBuilder subb;

	g_variant_builder_init (&b, G_VARIANT_TYPE_VARDICT);
	g_variant_builder_init (&subb, G_VARIANT_TYPE_VARDICT);

	g_variant_builder_add (&b, "{sv}", "number",
				g_variant_new_uint32 (1234));
	g_variant_builder_add (&b, "{sv}", "string",
				g_variant_new_string ("foobar"));
	g_variant_builder_add (&b, "{sv}", "string_num",
				g_variant_new_string ("1234"));
	g_variant_builder_add (&subb, "{sv}", "double",
				g_variant_new_double (3.1415));
	g_variant_builder_add (&subb, "{sv}", "bool",
				g_variant_new_boolean (TRUE));
	g_variant_builder_add (&b, "{sv}", "dict",
				g_variant_builder_end (&subb));
	return g_variant_builder_end (&b);
}

static GVariant *
build_auv ()
{
	GVariantBuilder b;

	g_variant_builder_init (&b, G_VARIANT_TYPE ("a{uv}"));
	g_variant_builder_add (&b, "{uv}", 1,
				g_variant_new_double (3.1415));
	g_variant_builder_add (&b, "{uv}", 7,
				g_variant_new_boolean (TRUE));
	g_variant_builder_add (&b, "{uv}", 18,
				g_variant_new_uint32 (1234));
	g_variant_builder_add (&b, "{uv}", 32,
				g_variant_new_string ("foobar"));
	return g_variant_builder_end (&b);
}

static GVariant *
build_array ()
{
	GVariantBuilder b;

	g_variant_builder_init (&b, G_VARIANT_TYPE ("as"));
	g_variant_builder_add (&b, "s", "one");
	g_variant_builder_add (&b, "s", "two");
	g_variant_builder_add (&b, "s", "three");
	g_variant_builder_add (&b, "s", "four");
	return g_variant_builder_end (&b);
}

int main (int argc, char **argv)
{
	AnatoleEngine *engine;
	GError *error = NULL;
	gboolean ret;

	setlocale (LC_ALL, "");

	g_test_init (&argc, &argv, NULL);

	engine = anatole_engine_new ("test");
	anatole_engine_add_function_inspect (engine);
	anatole_engine_add_function (engine,
				     "add",
				     add_int_func,
				     "(xx)",
				     NULL, NULL);
	anatole_engine_add_function (engine,
				     "multiply",
				     multiply_int_func,
				     "(xx)",
				     NULL, NULL);
	anatole_engine_add_function (engine,
				     "assert_equals_int",
				     assert_equals_int_func,
				     "(xx)",
				     NULL, NULL);
	anatole_engine_add_function (engine,
				     "string_contains",
				     string_contains_func,
				     "(ss)",
				     NULL, NULL);
	anatole_engine_add_function (engine,
				     "function_that_could_be_called",
				     function_that_could_be_called,
				     NULL,
				     NULL, NULL);
	anatole_engine_add_function (engine,
				     "send_asv",
				     send_asv_func,
				     "(a{sv})",
				     NULL, NULL);
	ret = anatole_engine_add_function_finish (engine, &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	ret = anatole_engine_load_script_from_path (engine,
						    SRCDIR "/test-anatole.lua",
						    &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	g_assert_true (function_called);

	check_global (engine, "global_int", "int64 1234567890");
	check_global (engine, "global_bool", "true");
	check_global (engine, "global_string", "'YAYAYAYAYAYA'");
	check_global (engine, "global_float", "3.1415899999999999");

	GVariant *variant = anatole_engine_get_global_variable (engine, "source");
	GVariant *id = g_variant_lookup_value (variant, "id", NULL);
	const char *id_str = g_variant_get_string (id, NULL);
	g_assert_cmpstr ("grl-video-title-parsing", ==, id_str);
	g_variant_unref (id);
	g_variant_unref (variant);

	ret = anatole_engine_call_function (engine, "modify_source", NULL, NULL);
	g_assert_true (ret);
	variant = anatole_engine_get_global_variable (engine, "source");
	id = g_variant_lookup_value (variant, "id", NULL);
	id_str = g_variant_get_string (id, NULL);
	g_assert_cmpstr ("source ID was changed", ==, id_str);
	g_variant_unref (id);
	g_variant_unref (variant);

	ret = anatole_engine_call_function (engine, "modify_source_with_name",
					    g_variant_new ("(ss)",
							   "source ID was changed again",
							   "global string changed"),
					    NULL);
	g_assert_true (ret);
	variant = anatole_engine_get_global_variable (engine, "source");
	id = g_variant_lookup_value (variant, "id", NULL);
	id_str = g_variant_get_string (id, NULL);
	g_assert_cmpstr ("source ID was changed again", ==, id_str);
	g_variant_unref (id);
	g_variant_unref (variant);
	check_global (engine, "global_string", "'global string changed'");

	ret = anatole_engine_call_function (engine, "receive_asv",
					    g_variant_new ("(@a{sv})",
							   build_asv ()),
					    NULL);
	g_assert_true (ret);

	ret = anatole_engine_call_function (engine, "receive_auv",
					    g_variant_new ("(@a{uv})",
							   build_auv ()),
					    NULL);
	g_assert_true (ret);

	ret = anatole_engine_call_function (engine, "receive_array",
					    g_variant_new ("(@as)",
							   build_array ()),
					    NULL);
	g_assert_true (ret);

	ret = anatole_engine_load_script_from_path (engine,
						    SRCDIR "/test-anatole-2.lua",
						    &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	check_global (engine, "global_string", "'test-anatole-2.lua wuz here'");

	g_object_unref (engine);

	return 0;
}
