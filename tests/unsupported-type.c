#include <locale.h>
#include "anatole.h"

static GVariant *
funcify_func (AnatoleEngine *engine, GVariant *variant, gpointer user_data)
{
	/* Nothing */
	return NULL;
}

int main (int argc, char **argv)
{
	AnatoleEngine *engine;
	GError *error = NULL;
	gboolean ret;

	setlocale (LC_ALL, "");

	g_test_init (&argc, &argv, NULL);

	engine = anatole_engine_new ("test");
	anatole_engine_add_function_inspect (engine);
	anatole_engine_add_function (engine,
				     "funcify",
				     funcify_func,
				     "(s)",
				     NULL, NULL);
	ret = anatole_engine_add_function_finish (engine, &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	g_test_expect_message (G_LOG_DOMAIN,
			       G_LOG_LEVEL_WARNING,
			       "*Unhandled type function*");

	ret = anatole_engine_load_script_from_path (engine,
						    SRCDIR "/unsupported-type.lua",
						    &error);
	g_assert_null (error);
	g_assert_true (ret);
	g_test_assert_expected_messages ();

	g_object_unref (engine);

	return 0;
}
