encoded = lib.percent_encode('Test/Test 2')
assert(encoded == 'Test%2FTest%202')
decoded = lib.percent_decode(encoded)
assert(decoded == 'Test/Test 2')
