#include <locale.h>
#include "anatole.h"

int main (int argc, char **argv)
{
	AnatoleEngine *engine;
	GError *error = NULL;
	gboolean ret;

	setlocale (LC_ALL, "");

	g_test_init (&argc, &argv, NULL);

	engine = anatole_engine_new ("lib");
	anatole_engine_add_function_inspect (engine);
	ret = anatole_engine_add_percent_encoding (engine);
	g_assert_true (ret);
	ret = anatole_engine_add_function_finish (engine, &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	ret = anatole_engine_load_script_from_path (engine,
						    SRCDIR "/percent-encoding.lua",
						    &error);
	g_assert_no_error (error);
	g_assert_true (ret);

	g_object_unref (engine);

	return 0;
}
