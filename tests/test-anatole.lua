source = {
  id = "grl-video-title-parsing",
  name = "video-title-parsing",
  description = "Video title parsing",
  supported_keys = { "episode-title", 'show', 'publication-date', 'season', 'episode', 'title' },
  supported_media = 'video',
  resolve_keys = {
    ["type"] = "video",
    required = { "title" },
  },
}

global_float = 3.14159
global_bool = true
global_string = "YAYAYAYAYAYA"
global_int = 1234567890

function modify_source()
	source.id = "source ID was changed"

	return "string that should really be ignored"
end

function modify_source_with_name(my_string, my_other_string)
	source.id = my_string
	global_string = my_other_string

	return "string that should really be ignored as well"
end

function receive_asv(dict)
	assert(dict.number == 1234)
	assert(dict.string == "foobar")
	assert(dict.string_num == "1234")
	assert(dict.dict.double == 3.1415)
	assert(dict.dict.bool == true)

	dict.double = dict.dict.double
	dict.bool = dict.dict.bool
	dict.dict = nil
	test.send_asv(dict)
end

function receive_auv(dict)
	assert(dict[18] == 1234)
	assert(dict[32] == "foobar")
	assert(dict[1] == 3.1415)
	assert(dict[7] == true)
end

function receive_array(array)
	assert(array[0] == "one")
	assert(array[1] == "two")
	assert(array[2] == "three")
	assert(array[3] == "four")
end

-- print(global_string)

item = {}
item.foo = '1'
item.bar = 1
item.baz = 2

item.out = test.add(item.bar, item.baz)
assert(item.out == item.bar + item.baz)
test.assert_equals_int(item.out, item.bar + item.baz)

item.out2 = test.multiply(item.bar, item.baz)
assert(item.out2 == item.bar * item.baz)
test.assert_equals_int(item.out2, item.bar * item.baz)

assert(test.string_contains("unsurpassed", "ass"))
assert(not test.string_contains("unsurrealism", "ass"))

-- print(test.inspect(item))

test.function_that_could_be_called()
